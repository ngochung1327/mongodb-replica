### Mongo replica same machine using docker
1. [Guide](https://www.youtube.com/watch?v=mlw7vWISaF4) and [document](https://docs.mongodb.com/manual/replication/)
2. Set up
```
romvarac@romvarac:~/future/learning/mongo$ make
romvarac@romvarac:~/future/learning/mongo$ docker-compose up --build
romvarac@romvarac:~/future/learning/mongo$ docker exec mongodb-replicaset_mongo-rs0-1_1 bash -c 'mongo --eval "rs.status();"'
```
